

#Constantes
param numBarredoras;
param H >= 0; #Maximo de pasos de las barredoras
param numPuntos;
param M >= 0; #Penalizacion por barrer calle

set Nodos := 1..numPuntos;
set Barredoras := 1..numBarredoras;
set Pasos = 1..H;
set calles within {Nodos cross Nodos};

#Nodos iniciales
param Cuartelillos {Barredoras};

#Calles iniciales
set callei {Barredoras};

#Matriz de distancias
param d {calles} >= 0;	#distancia de nodo a nodo


#esto luego
param CalleEspecial; 
param limInf;
param limSup;
#set Gasolineras;
#set recargaDeAgua;	# nodo por el que tenemos que pasar para recargar agua
						# NODO O CALLE?


# Xijkh = 1 si Barredora k va del nodo i al j en el paso h, BARRIENDO
var x {calles, Barredoras, Pasos}, binary;

# Yijkh = 1 si Barredora k va del nodo i al j en el paso h, SIN BARRER
var y {calles, Barredoras, Pasos}, binary;

# akh es el tiempo acumulado que lleva la barredora k en el paso h
var a {Barredoras, Pasos};# >= 0;

# Variable auxiliar para llevar la función objetivo mini max
var maximo;

# Funcion objetivo: minimizar el tiempo que tardan las barredoras en limpiar la zona asignada
minimize Objetivo: maximo;

/* Restricciones */

# Maximo mayor o igual que la distancia/tiempo de cada barredora
subject to MiniMax {k in Barredoras}:
	maximo >= a[k,H];

# Calcular el tiempo que lleva la barredora k en el paso h
subject to TiempoAcumulado {k in Barredoras, h in Pasos}:
	a[k,h] = sum {n in 1..h, (i,j) in calles} (d[i,j]*(x[i,j,k,n]*M + y[i,j,k,n]));

# Cada calle se barre una vez, distingue calles bidireccionales
subject to UnPasoUnaCalle {k in Barredoras, h in Pasos}:
	sum {(i,j) in calles} (x[i,j,k,h] + y[i,j,k,h]) <= 1; 

# Cada calle se barre una vez, distingue calles bidireccionales
subject to CadaCalleSeBarreUnaVez1 {(i,j) in calles : (j,i) in calles}:
	sum {k in Barredoras, h in Pasos} (x[i,j,k,h] + x[j,i,k,h]) = 1; 
# Cada calle se barre una vez
subject to CadaCalleSeBarreUnaVez2 {(i,j) in calles : (j,i) not in calles}:
	sum {k in Barredoras, h in Pasos} x[i,j,k,h] = 1; 

#Flujo
#Si una barredora pasa por un nodo, sale del mismo
#En el nodo inicial/final no.
subject to ConservacionFlujo {k in Barredoras, h in Pasos, j in Nodos : h > 1 and j <> Cuartelillos[k]}:
	sum {i in Nodos, (ii,j) in calles : i = ii} (x[i,j,k,h-1] + y[i,j,k,h-1]) = sum {i in Nodos, (j,ii) in calles : i = ii} (x[j,i,k,h] + y[j,i,k,h]);

#Flujo nodo origen
#Si una barredora pasa por un nodo, sale del mismo
#En el nodo inicial/final no.
subject to ConservacionFlujoI {k in Barredoras, (i,j) in calles : i = Cuartelillos[k]}:
	sum {h in Pasos} (x[i,j,k,h] + y[i,j,k,h]) = sum {h in Pasos} (x[j,i,k,h] + y[j,i,k,h]);

# Nodo origen
subject to PuntoInicial {k in Barredoras}:
	sum {j in Nodos, (Cuartelillos[k],jj) in calles : j = jj} (x[Cuartelillos[k],j,k,1] + y[Cuartelillos[k],j,k,1]) = 1;

/*
# Nodo destino, hay que llegar al cuartelillo
subject to PuntoFinal {k in Barredoras}:
	sum {j in Nodos, (jj,Cuartelillos[k]) in calles : j = jj} (x[j,Cuartelillos[k],k,H] + y[j,Cuartelillos[k],k,H]) = 1;
*/

#Nota: Duplicamos aristas en algun caso? (Si, revisar cuando sea, no es problema)
/*
# Obligar a pasar por la calle de recarga de agua solamente una vez
subject to PasoCalleAgua {k in Barredoras}:
	sum {h in Pasos} (x[CalleEspecial, k, h] + y[CalleEspecial, k, h])= 1;
	
*/

/*
# Obligar a pasar por la calle de 
subject to HorarioDePasoPorCalleAgua {k in Barredoras, h in Pasos}:
	(x[2, 8, k, h] + y[2, 8, k, h])*limInf <= a[k, h] + d[2, 8]*(x[2, 8, k, h]*M + y[2, 8, k, h]) <= limSup*(2 - (x[2, 8, k, h] + y[2, 8, k, h]));
*/	
	
# limInf
subject to LimInf {k in Barredoras, h in Pasos: h>1}:
	(x[20, 21, k, h] + y[20, 21, k, h])*limInf <= a[k, h-1] + d[20, 21]*(x[20, 21, k, h]*M + y[20, 21, k, h]);
	
# limSup
subject to LimSup {k in Barredoras, h in Pasos: h>1}:
	a[k, h-1] + d[20, 21]*(x[20, 21, k, h]*M + y[20, 21, k, h]) <= limSup*(2 - (x[20, 21, k, h] + y[20, 21, k, h]));

subject to PasoCalleAgua {k in Barredoras}:
	sum {h in Pasos} (x[20, 21, k, h] + y[20, 21, k, h]) >= 1;

	
	
	