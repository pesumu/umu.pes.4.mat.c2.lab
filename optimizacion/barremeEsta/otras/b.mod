

#Constantes
param numBarredoras;
param numPuntos;
param M >= 0; #Penalizacion por barrer calle

set Nodos := 1..numPuntos;
set Barredoras := 1..numBarredoras;
set calles within {Nodos cross Nodos};

#Nodos iniciales
param Cuartelillos {Barredoras};

#Matriz de distancias
param d {calles} >= 0;	#distancia de nodo a nodo

#esto luego
#param CalleEspecial; 
#param limInf;
#param limSup;
#set Gasolineras;
#set recargaDeAgua;	# nodo por el que tenemos que pasar para recargar agua
						# NODO O CALLE?

#Numero de veces que pasamos por una calle
# Xijk
var x {calles, Barredoras}, integer, >= 0;
#Variables para MTZ
#var u {Nodos, Barredoras}, >= 0;

# Funcion objetivo: minimizar el tiempo que tardan las barredoras en limpiar la zona asignada
minimize Objetivo: sum {k in Barredoras, (i,j) in calles} (d[i,j]*(x[i,j,k]));

/* Restricciones */

# Cada calle se barre una vez, distingue calles bidireccionales
# i < j para no repetir restricciones iguales
subject to CadaCalleSeBarreUnaVez1 {(i,j) in calles : (j,i) in calles and i < j}:
	sum {k in Barredoras} (x[i,j,k] + x[j,i,k]) >= 1;

# Cada calle se barre una vez
subject to CadaCalleSeBarreUnaVez2 {(i,j) in calles : (j,i) not in calles}:
	sum {k in Barredoras} x[i,j,k] >= 1;

#Restriccion de flujo POR NODO. Lo que entra es igual a lo que sale. Saldran subciclos.
#(Las barredoras son independientes!!)
subject to ConservacionFlujo {k in Barredoras, n in Nodos}:
	sum {(i,j) in calles: j = n} x[i,j,k] = sum {(i,j) in calles: i = n} x[i,j,k];
/*
#La barredora k sale del cuartelillo
subject to HayMovimiento {k in Barredoras}:
	sum {(i,j) in calles : i = Cuartelillos[k]} x[i,j,k] >= 1;
*/
/*
subject to ciclo:
	x[1,8,2] >= 1;
*/
/*
## RESTRICCIONES MTZ para evitar subciclos
subject to MTZ {k in Barredoras, (i,j) in calles : i != Cuartelillos[k]}:
	u[i,k] - u[j,k] + numPuntos*x[i,j,k] <= numPuntos - 1;
*/

	
