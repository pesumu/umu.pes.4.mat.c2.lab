##############
### Modelo ###
##############

#Parametros
param numBarredoras; #Numero de barredoras
param H >= 0; #Maximo de pasos de las barredoras
param numPuntos; #Numero de nodos
param aguai; #Calle del agua (aguai, aguaj)
param aguaj;
param duracionJornada >= 0;
param duracionDeposito >= 0;
param velocidadSinBarrer >= 0; # m/h
param velocidadBarriendo >= 0; # m/h

#Conjuntos
set Nodos := 1..numPuntos;
set Barredoras := 1..numBarredoras;
set Pasos = 1..H;
set calles within {Nodos cross Nodos}; #Aristas del grafo

#Datos afectados por las zonas barridas previamente
param tiempoBarridoInicial default 0;
param tiempoTotalInicial default 0;

#Parametros para multiobjetivo
param obj1; #Coeficiones f.o.
param obj2;
param maxVal; #Valor para fijar maximo

#Nodos iniciales
param Cuartelillos {Barredoras};
#Nodos finales
param NodoFinal {Barredoras};

#Matriz de distancias
param d {calles} >= 0;	#distancia de nodo a nodo

#Limites de tiempo para pasar por el agua
param limInf:=duracionJornada-duracionDeposito;
param limSup:=duracionDeposito;

###Variables###

# Xijkh = 1 si Barredora k va del nodo i al j en el paso h, BARRIENDO
var x {calles, Barredoras, Pasos}, binary;

# Yijkh = 1 si Barredora k va del nodo i al j en el paso h, SIN BARRER
var y {calles, Barredoras, Pasos}, binary;

# akh es el tiempo SIN BARRER que lleva la barredora k al final del paso h, en esta zona
var a {Barredoras, Pasos} >= 0;
# akh es el tiempo BARRIENDO que lleva la barredora k al final del paso h, en esta zona
var b {Barredoras, Pasos} >= 0;

# Maximo de los tiempos que tardar las barredoras en cerrar su recorrido
var maximo >= 0;
#Suma de los tiempos de las barredoras
var suma >= 0;

# Funcion objetivo: multiobjetivo, en una primera fase minimizamos maximo, en la segunda fase, la suma
minimize recorridoMultiobjetivo: obj1*maximo+obj2*suma;

/* Restricciones */

#Definicion de variables#

#Linealizacion del maximo
# Maximo mayor o igual que el tiempo de cada barredora
subject to Maximo {k in Barredoras}:
	maximo >= (a[k,H]+b[k,H]);

subject to Sumatorio:
	suma = sum{k in Barredoras} (a[k,H]+b[k,H]);

#Para la segunda fase
subject to maxFijo:
	maximo = maxVal;

# Calcular el tiempo que lleva la barredora k en el paso h
subject to TiempoAcumuladoSinBarrer {k in Barredoras, h in Pasos}:
	a[k,h] = sum {n in 1..h, (i,j) in calles} (d[i,j]*(y[i,j,k,n]/velocidadSinBarrer));

subject to TiempoAcumuladoBarriendo {k in Barredoras, h in Pasos}:
	b[k,h] = sum {n in 1..h, (i,j) in calles} (d[i,j]*(x[i,j,k,n]/velocidadBarriendo));

#Restricciones Problema Cartero Chino#

# En cada paso atravesamos (barriendo o sin barrer) una calle como mucho
subject to UnPasoUnaCalle {k in Barredoras, h in Pasos}:
	sum {(i,j) in calles} (x[i,j,k,h] + y[i,j,k,h]) <= 1;

# Cada calle se barre una vez, distingue calles bidireccionales
subject to CadaCalleSeBarreUnaVez1 {(i,j) in calles : (j,i) in calles}:
	sum {k in Barredoras, h in Pasos} (x[i,j,k,h] + x[j,i,k,h]) = 1; 

# Cada calle se barre una vez (calle unidireccional)
subject to CadaCalleSeBarreUnaVez2 {(i,j) in calles : (j,i) not in calles}:
	sum {k in Barredoras, h in Pasos} x[i,j,k,h] = 1; 

#Si una barredora pasa por un nodo, sale del mismo en el siguiente paso
#En el nodo final no es necesario (asi puede terminar su recorrido sin seguir dando vueltas).
#Garantizamos que la trayectoria es conexa
subject to ConservacionFlujo {k in Barredoras, h in Pasos, j in Nodos : h > 1 and j <> NodoFinal[k]}:
	sum {i in Nodos, (ii,j) in calles : i = ii} (x[i,j,k,h-1] + y[i,j,k,h-1]) = sum {i in Nodos, (j,ii) in calles : i = ii} (x[j,i,k,h] + y[j,i,k,h]);


#Condiciones iniciales y finales de las barredoras

#Partimos del nodo origen
subject to PuntoInicial {k in Barredoras}:
	sum {j in Nodos, (Cuartelillos[k],jj) in calles : j = jj} (x[Cuartelillos[k],j,k,1] + y[Cuartelillos[k],j,k,1]) = 1;

#Esto se cumplira solamente si
#1. Llega al j = Final, es decir, acaba en el nodo final
#2. Ya estaba en el nodo final (asi que se puede quedar parado
subject to PuntoFinal {k in Barredoras, (i,j) in calles: j <> NodoFinal[k]}:
	x[i,j,k,H] = 0;
subject to PuntoFinal2 {k in Barredoras, (i,j) in calles: j <> NodoFinal[k]}:
	y[i,j,k,H] = 0;


#Restricciones de la empresa#
	
# No puede superarse la duracion de la jornada
subject to LimiteJornada {k in Barredoras}:
	a[k,H]+b[k,H]+tiempoTotalInicial <= duracionJornada;

#Variables fijables
#Acabamos en el nodo final
subject to PuntoFin {k in Barredoras, (i,j) in calles : j <> NodoFinal[k]}:
	x[i,j,k,H] = 0;
subject to PuntoFin2 {k in Barredoras, (i,j) in calles : j <> NodoFinal[k]}:
	y[i,j,k,H] = 0;

# Restricciones del agua
# limInf
subject to LimInf {k in Barredoras, h in Pasos: h>1}:
	(x[aguai, aguaj, k, h]+y[aguai, aguaj, k, h])*limInf <= b[k, h-1]+tiempoBarridoInicial + d[aguai, aguaj]*(x[aguai, aguaj, k, h]/velocidadBarriendo);

# limSup
subject to LimSup {k in Barredoras, h in Pasos: h>1}:
	b[k, h-1]+tiempoBarridoInicial + d[aguai, aguaj]*(x[aguai, aguaj, k, h]/velocidadBarriendo) <= limSup*(2 - (x[aguai, aguaj, k, h]+y[aguai, aguaj, k, h]));

# Todas las barredoras deben pasar por la calle del agua
subject to PasoCalleAgua {k in Barredoras}:
	sum {h in Pasos} (x[aguai, aguaj, k, h] + y[aguai, aguaj, k, h]) >= 1;

# La calle del cuartelillo real NO va a barrerse (es autopista)
# Aplicara solo en una de las zonas
subject to AristaCuartelilloSucia {k in Barredoras, h in Pasos, (i,j) in calles : i = Cuartelillos[k] or j = Cuartelillos[k]} :
	x[i,j,k,h] = 0;

# Restricciones opcionales

#Como todas las barredoras pasaran por el agua, diremos que barra la numero 1
#Ojo, esto no implica optimalidad, el peso de la calle deberia no ser grande para ello
subject to PasoCalleAguaEntra {k in Barredoras, h in Pasos : k > 1}:
	x[aguai,aguaj,k,h] = 0;
#Al salir de la calle del agua, no hace falta que barra nadie 
subject to PasoCalleAguaSale {k in Barredoras, h in Pasos}:
	x[aguaj,aguai,k,h] = 0;
