# Formulacion para el problema "Modelo clasico del transporte"

set ORIGENES;
set DESTINOS;

param costes {ORIGENES,DESTINOS};
param costesFijo {ORIGENES, DESTINOS};

param oferta {ORIGENES};
param costini {ORIGENES};
param demanda {DESTINOS};


var x{ORIGENES,DESTINOS}>=0,integer;
var y{ORIGENES,DESTINOS}, binary;

# check: sum{i in ORIGENES} oferta[i] >= sum {j in DESTINOS} demanda[j];

minimize coste_total: sum{i in ORIGENES, j in DESTINOS} (costes[i,j]+costini[i])*x[i,j]+costesFijos[i,j]*y[i,j];

subject to ofer {i in ORIGENES}: sum{j in DESTINOS} x[i,j] <=oferta[i];

subject to dema {j in DESTINOS}: sum{i in ORIGENES} x[i,j]>=demanda[j];

subject to hayEnvio {i in ORIGENES, j in DESTINOS}: demanda[j]*y[i,j]>= x[i,j]; 