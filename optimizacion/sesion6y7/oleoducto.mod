# Modelo oleoductos

set NODOS;

param origen symbolic in NODOS;           # entrance to road network
param destino symbolic in NODOS, <> origen;  # exit from road network


set ARCOS within (NODOS  diff  {destino}) cross (NODOS diff {origen});

param capacidad {ARCOS}>=0;

var flujo {(i,j) in ARCOS}>=0, <=capacidad[i,j];

maximize ft: sum {(origen,j) in ARCOS} flujo[origen,j];

subject to conservflujo {k in NODOS diff {origen,destino}}:
  sum {(i,k) in ARCOS} flujo[i,k] = sum {(k,j) in ARCOS} flujo[k,j];