# Formulación para el problema "Modelo clásico de asignación"

set DOCUMENTOS; 
set AUXILIARES;

param tiempo {AUXILIARES,DOCUMENTOS};

var x {AUXILIARES,DOCUMENTOS} binary;

minimize tiempo_total: sum{i in AUXILIARES, j in DOCUMENTOS} tiempo[i,j]*x[i,j];

subject to trab {i in AUXILIARES}: sum{j in DOCUMENTOS} x[i,j]=1;
subject to tarea {j in DOCUMENTOS}: sum{i in AUXILIARES} x[i,j]=1;
