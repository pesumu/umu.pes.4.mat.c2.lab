package mapasMod;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultUndirectedWeightedGraph;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

/**
 * 
 */
public class Main {


	/**
	 * 
	 * @param filePath
	 * @return
	 * @throws java.io.IOException
	 */
	private static String readFileAsString(String filePath) throws java.io.IOException {
		StringBuffer fileData = new StringBuffer(1000);
		BufferedReader reader = new BufferedReader(new FileReader(filePath));
		char[] buf = new char[1024];
		int numRead = 0;
		while ((numRead = reader.read(buf)) != -1) {
			String readData = String.valueOf(buf, 0, numRead);
			fileData.append(readData);
			buf = new char[1024];
		}
		reader.close();
		return fileData.toString();
	}

	/**
	 * main method that simply reads some nodes
	 * 
	 * @param args
	 * @throws IOException
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 * @throws TransformerException 
	 */
	public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException, TransformerException {
		
		Plano planoMurcia = new Plano();
		
		
		File fXmlFile = new File("zonaMapa1.xml");
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(fXmlFile);
				
		//optional, but recommended
		//read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
		doc.getDocumentElement().normalize();

		System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
				
		NodeList nList = doc.getElementsByTagName("coordinates");
				
		System.out.println("----------------------------");
		
		
		
		
		for (int temp = 0; temp < nList.getLength(); temp++) {

			Node nNode = nList.item(temp);
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				Punto pNuevo1 = null;
				Punto pNuevo2 = null;
				
				Element eElement = (Element) nNode;
				
				String[] coordenadas = eElement.getTextContent().split(",0");

				for(int i = 0; i<coordenadas.length;i++) {
					String texto = coordenadas[i].replace("\n","").trim();
					if(texto.length()==0)
						continue;
					if(pNuevo1==null) {
						pNuevo1 = planoMurcia.insertarSiNoExiste(texto);
					}else {
						pNuevo2 = pNuevo1;
						pNuevo1 = planoMurcia.insertarSiNoExiste(texto);
						planoMurcia.insertarAristaSiNoExiste(new Arista(pNuevo2, pNuevo1));
					}
				}
			}
		}
		
		System.out.println(planoMurcia.getPuntosSobrantes().size());
		System.out.println(planoMurcia.getPuntos().size());
		//En planoMurcia esta el plano original
		
		//Reduciendo el numero de aristas y nodos
		Plano reducido = planoMurcia.compactar();
		
		/*
		System.out.println("Punto de entrada: "+planoMurcia.insertarSiNoExiste("-1.1230727,37.9739095").id);
		System.out.println("Cuartelillo del centro: "+planoMurcia.insertarSiNoExiste("-1.1304068,37.9794022").id);
		*/
		
		/**
		 * Informaci�n del mapa de Mangas
		 
		System.out.println("Punto de entrada: "+planoMurcia.insertarSiNoExiste("-1.13033,37.97939").id);
		System.out.println("Punto de entrada: "+planoMurcia.insertarSiNoExiste("-1.1321898,37.9772614").id);
		System.out.println("Cuartel Norte: "+planoMurcia.insertarSiNoExiste("-1.13046,37.98215").id);
		System.out.println("Cuartel sur: "+planoMurcia.insertarSiNoExiste("-1.12376,37.97117").id);
		*/
		//System.out.println("Salida: "+planoMurcia.insertarSiNoExiste("-1.12722,37.9948676").id);
		
		
		
		//Fichero de salida
		PrintStream out = System.out;
		PrintStream fileStream = new PrintStream("a.dat");
		System.setOut(fileStream);
		
		//Mostramos la salida
		reducido.toStringv4();
		//planoMurcia.toStringv4();
		//Usar v3 si queremos que el grafo salida sea bidireccional siempre
		
//		
		System.setOut(out);
		
		new CreadorKML(planoMurcia, "salida1.txt");

		
		System.out.println("\nFIN EJECUCION");
	}
	
}

