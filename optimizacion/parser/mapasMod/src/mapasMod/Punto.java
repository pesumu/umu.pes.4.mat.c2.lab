package mapasMod;

public class Punto {
	
	//Umbral para identificar puntos cercanos
	public static final double umbral=0.75;
	private double lat, lon;
	
	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int id;
	public int idAntiguo;

	//Devuelve nueva copia del punto. Mejor cambiar la id luego
	public Punto (Punto p) {
		lat = p.getLat();
		lon = p.lon;
		id = p.id;
	}
	
	public Punto(double latitud, double longitud, int idd) {
		lat = latitud;
		lon = longitud;
		id = idd;
	}
	
	public Punto(String latitud, String longitud) {
		lat = Double.parseDouble(latitud);
		lon = Double.parseDouble(longitud);
	}

	public boolean muyCercano(Punto p1) {
		return distancia(p1)<=umbral;
	}

	public double distancia(Punto p1) {

		double lat1 = lat;
		double lon1 = lon;

		double lat2 = p1.lat;
		double lon2 = p1.lon;

		final int R = 6371; // Radius of the earth

		double latDistance = Math.toRadians(lat2 - lat1);
		double lonDistance = Math.toRadians(lon2 - lon1);
		double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + Math.cos(Math.toRadians(lat1))
				* Math.cos(Math.toRadians(lat2)) * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double distance = R * c * 1000; // convert to meters

		double height = 0;

		distance = Math.pow(distance, 2) + Math.pow(height, 2);

		return Math.sqrt(distance);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	//equals por ID unicamente
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Punto other = (Punto) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public String toString(){
		return lat+","+lon+",0";
	}
}
