package mapasMod;

import java.util.LinkedList;

public class Arista {
	private Punto p1,p2;
	private double distancia;

	public LinkedList<Punto> cadena = new LinkedList<>();

	public Arista(Punto p1, Punto p2) {
		super();
		this.p1 = p1;
		this.p2 = p2;
		this.distancia = p1.distancia(p2);
		cadena.add(p1);
		cadena.add(p2);
	}
	
	public Arista(Punto punto, Punto punto2, double suma) {
		this.p1=punto;
		this.p2=punto2;
		this.distancia=suma;
	}
	
	public Arista(Arista a1, Arista a2) {
		this.p1=a1.getP1();
		this.p2=a2.getP2();
		this.distancia=a1.distancia()+a2.distancia();
		cadena.addAll(a1.cadena);
		cadena.addAll(a2.cadena);
	}

	public Arista() {
	}

	public void setP1(Punto p1) {
		this.p1 = p1;
	}

	public void setP2(Punto p2) {
		this.p2 = p2;
	}

	public void setDistancia(double distancia) {
		this.distancia = distancia;
	}

	public Punto getP1() {
		return p1;
	}

	public Punto getP2() {
		return p2;
	}

	public double distancia() {
		return distancia;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((p1 == null) ? 0 : p1.hashCode());
		result = prime * result + ((p2 == null) ? 0 : p2.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Arista other = (Arista) obj;
		if (p1 == null) {
			if (other.p1 != null)
				return false;
		} else if (!p1.equals(other.p1))
			return false;
		if (p2 == null) {
			if (other.p2 != null)
				return false;
		} else if (!p2.equals(other.p2))
			return false;
		return true;
	}
}
