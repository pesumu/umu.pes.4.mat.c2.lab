package mapasMod;

import java.io.*;
import java.nio.file.NotDirectoryException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class CreadorKML {

	private Plano planoOriginal;
	private Plano planoCompacto;


	public CreadorKML(Plano plano, String nombreFichero) throws ParserConfigurationException, TransformerException{
		this.planoOriginal = plano;
		this.planoCompacto = plano.compactar();
		BufferedReader reader;

		String coordenadas = "";
		
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		Document doc = docBuilder.newDocument();
		
		Element rootElement = doc.createElement("kml");
		rootElement.setAttribute("xmlns", "http://www.opengis.net/kml/2.2");
		doc.appendChild(rootElement);
		
		Element documentElement = doc.createElement("Document");
		rootElement.appendChild(documentElement);
		
		Element nameElement = doc.createElement("name");
		nameElement.appendChild(doc.createTextNode("Mapa con las rutas"));
		documentElement.appendChild(nameElement);
		
		int k = 0;
		
		int paso = 0;
		
		Element folderElement = null;
		
		int numBarredoras = 0;

		try{
			reader = new BufferedReader(new FileReader(nombreFichero));
			String line = reader.readLine();
			
			// Primero contamos el número de barredoras
			while(line!=null){
				if(line.equals("-1")){
					numBarredoras++;
				}
				line = reader.readLine();
			}
			
			
			Element[] capas = new Element[numBarredoras]; // Una capa por barredora
			
			
			reader = new BufferedReader(new FileReader(nombreFichero));
			line = reader.readLine();
			
			while(line!=null){

				if(line.equals("-1")){
					k++;
					documentElement.appendChild(generateStyle(doc, "ffff0000", "2"));
					documentElement.appendChild(generateStyle(doc, "ffff00ff", "10"));
					capas[k-1]=doc.createElement("Folder");
					Element nameBarredora = doc.createElement("name");
					nameBarredora.appendChild(doc.createTextNode("Barredora "+k));
					capas[k-1].appendChild(nameBarredora);
					documentElement.appendChild(capas[k-1]);					
					
					paso=0;
				}else{
					paso++;
					coordenadas = "";
					// Ahora tenemos que buscar la arista y sustituirla por su cadena correspondiente
					// Las aristas nos llegan con los ids nuevos
					//String []splits = line.replaceAll("^\\s*\\(|\\)\\s*$", "").split("\\s*,\\s*");
					String[] splits = line.split(" ");
					Punto p1 = planoCompacto.buscaId(Integer.valueOf(splits[0]));
					Punto p2 = planoCompacto.buscaId(Integer.valueOf(splits[1]));
					int barre = Integer.valueOf(splits[2]);
					// Está la arista con estos ids antiguos en el plano original¿?
					Arista a = planoCompacto.insertarAristaSiNoExiste(new Arista(p1, p2));
					if(a!=null){
						// Podemos añadir la arista tal cual
						for(Punto p : a.cadena) {
							coordenadas+=p.toString()+"\n";
						}
						capas[k-1].appendChild(generateLine(doc, coordenadas, paso,barre==1?"BARRE":"NO BARRE",barre==1?"ffff0000":"ffff00ff", barre==1? "10":"2"));
					}
				}

				line = reader.readLine();
				
			}
			
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File("zonaMapaSALIDA1.kml"));

			// Output to console for testing
			// StreamResult result = new StreamResult(System.out);

			transformer.transform(source, result);
			
			reader.close();
		}catch(IOException e){
			e.printStackTrace();
		}
		
		

	}
	
	public Element generateStyle(Document doc,String color,String width) {
		
		Element style = doc.createElement("Style");
		style.setAttribute("id", "line-"+color+"-"+width);
		
		Element lineStyle = doc.createElement("LineStyle");
		
		style.appendChild(lineStyle);
		
		Element colorElement = doc.createElement("color");
		Element widthElement = doc.createElement("width");		
		colorElement.appendChild(doc.createTextNode("#"+color));
		widthElement.appendChild(doc.createTextNode(width));
		lineStyle.appendChild(colorElement);
		lineStyle.appendChild(widthElement);
		
		Element balloonStyle = doc.createElement("BalloonStyle");
		lineStyle.appendChild(balloonStyle);
		
		Element text = doc.createElement("text");
		text.appendChild(doc.createTextNode("<![CDATA[<h3>$[name]</h3>]]>"));
		
		balloonStyle.appendChild(text);
		
		
		
		
		
		return style;
	}
	
public Element generateLine(Document doc,String coordenadas,int numPaso,String barreONO,String color,String width) {
		
		Element placemark = doc.createElement("Placemark");
		
		Element name = doc.createElement("name");
		name.appendChild(doc.createTextNode("Paso "+numPaso+" - "+barreONO));		
		placemark.appendChild(name);
		
		Element style = doc.createElement("styleUrl");
		style.appendChild(doc.createTextNode("#line-"+color+"-"+width));		
		placemark.appendChild(style);
		
		Element lineString = doc.createElement("LineString");
		placemark.appendChild(lineString);
		
		Element tessellate = doc.createElement("tessellate");
		tessellate.appendChild(doc.createTextNode("1"));
		lineString.appendChild(tessellate);
		
		
		Element coordinates = doc.createElement("coordinates");
		coordinates.appendChild(doc.createTextNode(coordenadas));
		lineString.appendChild(coordinates);
				
		return placemark;
	}

}