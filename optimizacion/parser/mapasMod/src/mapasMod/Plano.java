package mapasMod;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.jgrapht.alg.connectivity.BiconnectivityInspector;
import org.jgrapht.graph.DefaultUndirectedWeightedGraph;
import org.jgrapht.graph.DefaultWeightedEdge;

public class Plano {
	private List<Punto> puntos = new LinkedList<>();
	private List<Arista> aristas = new LinkedList<>();
	private static final double EPS = 1E-10;
	
	public List<Punto> getPuntos() {
		return puntos;
	}

	public void setPuntos(List<Punto> puntos) {
		this.puntos = puntos;
	}

	public List<Arista> getAristas() {
		return aristas;
	}

	public void setAristas(List<Arista> aristas) {
		this.aristas = aristas;
	}

	public Punto insertarSiNoExiste(String texto) {
		texto = texto.replace("\n", "").trim();

		String[] separacion = texto.split(",");
		String latitud = separacion[0];
		String longitud = separacion[1];
		Punto punto = new Punto(latitud, longitud);
		// Buscar en la lista un punto muy cercano
		for (Punto p : puntos) {
			if (p.muyCercano(punto)) {
				return p;
			}
		}

		// Si no se encuentra ninguno
		puntos.add(punto);
		punto.id = puntos.size();
		return punto;
	}

	public Arista insertarAristaSiNoExiste(Arista a) {

		for (Arista arista : aristas) {
			if (arista.getP1().muyCercano(a.getP1()) & arista.getP2().muyCercano(a.getP2())) {
				// if (arista.getP2().muyCercano(a.getP1()) ||
				// arista.getP2().muyCercano(a.getP2())) { Para que funcionen los sentidos hay
				// que quitar esto
				return arista;
				// }
			}
		}
		if (a.getP1().id == a.getP2().id) {
			return null;
		}
		aristas.add(a);

		return a;
	}

	public List<Punto> getVecinos(Punto punto) {
		List<Punto> vecinos = new ArrayList<>();
		for (Arista arista : aristas) {
			if (arista.getP1() == punto) {
				vecinos.add(arista.getP2());
			} else if (arista.getP2() == punto) {
				vecinos.add(arista.getP1());
			}
		}
		return vecinos;
	}

	public List<Punto> getPuntosSobrantes() {
		List<Punto> resultado = new ArrayList<>();

		for (Punto p : puntos) {
			if (getVecinos(p).size() == 2)
				resultado.add(p);
		}
		return resultado;
	}

	// Renombra los puntos (su id) a partir de 1
	public void renombraPuntos() {
		int cnt = 1; // contador de IDs
		for (Punto p : puntos) {
			p.id = cnt++;
		}
	}

	// Return Punto con esa id. Null si no esta
	public Punto buscaId(List<Punto> l, int id) {
		for (Punto punto : l) {
			if (punto.id == id) {
				return punto;
			}
		}
		return null;
	}

	public Punto buscaId(int id) {
		for (Punto punto : puntos) {
			if (punto.id == id) {
				return punto;
			}
		}
		return null;
	}

	public Punto buscaIdAntiguo(int id) {
		for (Punto punto : puntos) {
			if (punto.idAntiguo == id) {
				return punto;
			}
		}
		return null;
	}

	public Arista estaAristaAntigua(int id1, int id2) {
		for (Arista arista : aristas) {
			if (arista.getP1().idAntiguo == id1 & arista.getP2().idAntiguo == id2) {
				return arista;
			} else if (arista.getP1().idAntiguo == id2 & arista.getP2().idAntiguo == id1) {
				return arista;
			}
		}
		return null;
	}

	public Arista estaArista(int id1, int id2) {
		for (Arista arista : aristas) {
			if (arista.getP1().id == id1 & arista.getP2().id == id2) {
				return arista;
			}
		}
		return null;
	}

	// Nuevo mapa con nodos renombrados, y aristas con esos mismos nodos
	public Plano renombrado() {
		Plano plano = new Plano();
		// ids[id] = nuevaId
		Hashtable ids = new Hashtable();
		LinkedList<Punto> puntos2 = new LinkedList<>();
		int count = 1;
		// Nuevo conjunto de puntos con nuevo ID
		for (Punto punto : puntos) {
			// Supongo identificador unico
			punto.idAntiguo = punto.id;
			ids.put(punto.id, count);
			puntos2.add(new Punto(punto.getLat(), punto.getLon(), count));
			count++;
		}
		LinkedList<Arista> aristas2 = new LinkedList<>();
		for (Arista a : aristas) {
			// Para cada arista, creamos una nueva arista, con extremos en los nuevos puntos
			Arista a2 = new Arista();
			int id1 = (int) ids.get(a.getP1().id);
			Punto p1 = buscaId(puntos2, id1);
			if (p1 == null)
				System.out.println("PROBLEMA al renombrar");
			else {
				a2.setP1(p1);
			}
			int id2 = (int) ids.get(a.getP2().id);
			Punto p2 = buscaId(puntos2, id2);
			if (p2 == null)
				System.out.println("PROBLEMA al renombrar");
			else {
				a2.setP2(p2);
			}
			a2.setDistancia(a.distancia());
			aristas2.add(a2);
		}
		plano.setPuntos(puntos2);
		plano.setAristas(aristas2);
		return plano;
	}

	/**
	 * Para cada nodo de grado 2, lo identificamos con uno de sus vecinos
	 * 
	 * @return
	 */
	public Plano compactar() {
		boolean depurar = false; // poner a 1 si depurar
		// debug en fichero de salida debugcompactar.txt
		Plano plano2 = new Plano();
		List<Punto> puntos2 = new ArrayList<>(puntos);
		List<Arista> aristas2 = new ArrayList<>(aristas);
		
		//DEPURAR
		PrintStream err = System.err;
		if (depurar) {
			PrintStream fileStream = null;
			try {
				fileStream = new PrintStream("debugCompactar.dat");
			} catch (FileNotFoundException e) {
			}
			System.setErr(fileStream);
		}

		int conta = 1;
		
		// Para cada punto, ver si tiene 2 vecinos
		for (Punto pto : puntos) {
			
			plano2.setAristas(aristas2);
			plano2.setPuntos(puntos2);
			
			//Depurar
			if (depurar) {
				System.err.println(plano2.toStringv5());
				System.err.println("\nPaso " + conta++ + " PUNTO " + pto.id);
			}
			
			Set<Punto> vecinos = new HashSet<>(); // Vecinos
			List<Arista> conexiones = new LinkedList<>(); // Conexiones con vecinos
			for (Arista arista : aristas2) { // GetVecinos, y las aristas
				if (arista.getP1() == pto) {
					vecinos.add(arista.getP2());
					conexiones.add(arista);
				} else if (arista.getP2() == pto) {
					vecinos.add(arista.getP1());
					conexiones.add(arista);
				}
			}
			//Ya tenemos sus nodos vecinos y las aristas incidentes
			if (vecinos.size() == 2) { // Nodo candidato a quitar
				Punto [] vecinos2 = (Punto[]) vecinos.toArray(new Punto[vecinos.size()]);
				if (vecinos2.length != 2) {
					System.err.println("Avisar por el grupo xD");
				}
				int aristaAdy = conexiones.size();
				//Quitable si:
				//1. 2 aristas adyacentes -> nodo ->
				//2. 4 aristas <-> nodo <->
				if (aristaAdy != 2 && aristaAdy != 4) {
					// No tiene sentido quitar el nodo (caso 3 aristas)
				} 
				else if ((aristaAdy==2) 
						&& (conexiones.get(0).getP1() == pto) && (conexiones.get(1).getP1() == pto)) {
					// No se puede quitar (las 2 aristas salen del nodo!)
					// <- nodo ->
				} else if ((aristaAdy==2)
						&& (conexiones.get(0).getP2() == pto) && (conexiones.get(1).getP2() == pto)) {
					// No se puede quitar (las 2 aristas entran al nodo!)
					// -> nodo <-
				} else {
					//Caso 2 aristas, 2 nodos
					if (aristaAdy == 2) {
						double suma = conexiones.get(0).distancia() + conexiones.get(1).distancia();
						Arista a;
						if (conexiones.get(0).getP1() == pto) {
							a = new Arista(conexiones.get(1).getP1(), conexiones.get(0).getP2(), suma);
							a.cadena.addAll(conexiones.get(1).cadena);
							a.cadena.addAll(conexiones.get(0).cadena);
						} else {
							a = new Arista(conexiones.get(0).getP1(), conexiones.get(1).getP2(), suma);
							a.cadena.addAll(conexiones.get(0).cadena);
							a.cadena.addAll(conexiones.get(1).cadena);
						}
						if (!aristas2.contains(a)) {
							puntos2.remove(pto);
							//Borra las 2 aristas
							for (Arista arista : conexiones) {
								aristas2.remove(arista);
							}
							aristas2.add(a);
						} // Ojo, si ya hubiera una arista entre los 2 vecinos, no podemos quitar el nodo
							// :(
					}
					//Caso 4 aristas 2 nodos
					else if (aristaAdy == 4) {
						//No sabemos como estan ordenadas las aristas de conexiones
						//Distancias de las aristas 1 y 2
						double d1 = conexiones.get(0).distancia();
						double d2 = conexiones.get(1).distancia();
						//Peso nueva arista
						double suma = 0;
						//Si d1 = d2, o bien los pesos de las aristas son iguales
						//O d1 y d2 son la misma arista (pero reciproca)
						if (d1 - d2 <= EPS) {
							suma = d1 + conexiones.get(2).distancia();
						} else {
							suma = d1 + d2;
						}
						//Arista y su opuesta
						Arista a = new Arista(vecinos2[0], vecinos2[1], suma);
						Arista b = new Arista(vecinos2[1], vecinos2[0], suma);
						
						//Chequeo: entre los 2 extremos NO habia ninguna arista
						if (!aristas2.contains(a) && !aristas2.contains(b)) {
							// Mete cadena de -> nodo
							for (Arista arista : conexiones) {
								if (a.getP1() ==  arista.getP1()) {
									a.cadena.addAll(arista.cadena);
								}
							}
							// Mete cadena de nodo ->
							for (Arista arista : conexiones) {
								if (a.getP2() ==  arista.getP2()) {
									a.cadena.addAll(arista.cadena);
								}
							}
							// Mete cadena de nodo <-
							for (Arista arista : conexiones) {
								if (b.getP1() ==  arista.getP1()) {
									b.cadena.addAll(arista.cadena);
								}
							}
							// Mete cadena de <- nodo
							for (Arista arista : conexiones) {
								if (b.getP2() ==  arista.getP2()) {
									b.cadena.addAll(arista.cadena);
								}
							}
							
							//Sustituye punto intermedio
							puntos2.remove(pto);
							//Borra las 4 aristas
							for (Arista arista : conexiones) {
								aristas2.remove(arista);
							}
							//Nueva arista y reciproco
							aristas2.add(a);
							aristas2.add(b);
						}
					}
				}
			}
		}
		plano2.setAristas(aristas2);
		plano2.setPuntos(puntos2);

		//FIN DEBUG
		if (depurar) {
			System.setErr(err);
		}		

		return plano2/* .renombrado() */;
	


	}

	// Redefinicion de toString

	// NO se usa
	public String toString() {
		String resultado = "";

		/*
		 * resultado += "N�mero de nodos: " + puntos.size(); resultado += "\n";
		 * resultado += "N�mero de aristas: " + aristas.size(); resultado += "\n";
		 * double longitud = 0; for (Arista arista : aristas) { longitud +=
		 * arista.distancia(); } resultado += "Longitud total de las aristas: " +
		 * longitud; resultado += "\n";
		 */

		System.out.print("param numPuntos:= " + String.valueOf(puntos.size()) + ";\n");

		System.out.print("param matriz:");
		System.out.print("\t");

		for (int i = 1; i <= puntos.size(); i++) {
			System.out.print(String.valueOf(i) + " ");
		}
		System.out.print(":=");

		for (int i = 1; i <= puntos.size(); i++) {

			System.out.print("\n");
			System.out.print(String.valueOf(i) + "\t");

			List<Punto> vecinos = getVecinos(puntos.get(i - 1));

			for (int j = 1; j <= puntos.size(); j++) {
				if (j == i) {
					System.out.print("0");
				} else if (vecinos.contains(puntos.get(j - 1))) {
					System.out.print(String.valueOf(puntos.get(i - 1).distancia(puntos.get(j - 1))));
				} else {
					System.out.print("-1");
				}
				System.out.print(" ");
			}

		}

		System.out.print(";");

		return resultado;
	}

	// No se usa
	public String toStringv2() {
		System.out.print("param numPuntos:= " + String.valueOf(puntos.size()) + ";\n");

		System.out.print("param matriz := ");
		for (Arista arista : aristas) {
			System.out.print("(" + arista.getP1().id + "," + arista.getP1().id + "," + arista.distancia() + ")\n");
		}
		return "";
	}

	// Version que DUPLICA aristas (la salida sera un grafo NO dirigido,
	// bidireccional)
	// (usar si queremos un grafo bidireccional siempre)
	public String toStringv3() {
		System.out.print("param numPuntos:= " + String.valueOf(puntos.size()) + ";\n");

		System.out.print("set calles:= ");
		for (int i = 0; i < aristas.size(); i++) {
			Arista arista = aristas.get(i);
			System.out.print("(" + arista.getP1().id + "," + arista.getP2().id + ")");
			// Mete la arista (j,i), si no se va a meter ya
			if (!aristas.contains(new Arista(arista.getP2(), arista.getP1(), arista.distancia())))
				System.out.print(", (" + arista.getP2().id + "," + arista.getP1().id + ")");
			if (i != aristas.size() - 1)
				System.out.print(", ");
		}
		System.out.print(";");
		System.out.println("");
		System.out.print("param d := ");
		for (Arista arista : aristas) {
			System.out.print(arista.getP1().id + " " + arista.getP2().id + " " + arista.distancia() + "\n");
			// Mete la arista (j,i), si no se va a meter ya
			if (!aristas.contains(new Arista(arista.getP2(), arista.getP1(), arista.distancia())))
				System.out.print(arista.getP2().id + " " + arista.getP1().id + " " + arista.distancia() + "\n");
		}
		System.out.println(";");
		return "";
	}

	// Version que NO duplica aristas (la salida sera el grafo dirigido)
	public String toStringv4() {
		System.out.print("param numPuntos:= " + String.valueOf(puntos.size()) + ";\n");

		System.out.print("set calles:= ");
		for (int i = 0; i < aristas.size(); i++) {
			Arista arista = aristas.get(i);
			System.out.print("(" + arista.getP1().id + "," + arista.getP2().id + ")");
			if (i != aristas.size() - 1)
				System.out.print(", ");
		}
		System.out.print(";");
		System.out.println("");
		System.out.print("param d := ");
		for (Arista arista : aristas) {
			System.out.print(arista.getP1().id + " " + arista.getP2().id + " " + arista.distancia() + "\n");
		}
		System.out.println(";");
		return "";
	}

	//toStringv4 pero a la salida de error (Debug)
	public String toStringv5() {
		PrintStream out = System.out;
		System.setOut(System.err);
		System.out.print("param numPuntos:= " + String.valueOf(puntos.size()) + ";\n");

		System.out.print("set calles:= ");
		for (int i = 0; i < aristas.size(); i++) {
			Arista arista = aristas.get(i);
			System.out.print("(" + arista.getP1().id + "," + arista.getP2().id + ")");
			if (i != aristas.size() - 1)
				System.out.print(", ");
		}
		System.out.print(";");
		System.out.println("");
		System.out.print("param d := ");
		for (Arista arista : aristas) {
			System.out.print(arista.getP1().id + " " + arista.getP2().id + " " + arista.distancia() + "\n");
		}
		System.out.println(";");

		System.setOut(out);
		return "";
	}
	
	// IGNORAR
	/*
	 * public Plano getReducido() {
	 * 
	 * DefaultUndirectedWeightedGraph<Punto, DefaultWeightedEdge> grafo = new
	 * DefaultUndirectedWeightedGraph<>( DefaultWeightedEdge.class);
	 * 
	 * for (Punto p : puntos) { grafo.addVertex(p); }
	 * 
	 * for (Arista arista : aristas) { grafo.addEdge(arista.getP1(),
	 * arista.getP2()); grafo.setEdgeWeight(arista.getP1(), arista.getP2(),
	 * arista.distancia()); }
	 * 
	 * BiconnectivityInspector<Punto, DefaultWeightedEdge> inspector = new
	 * BiconnectivityInspector<>(grafo);
	 * 
	 * for (Punto pto : puntos) { if (grafo.degreeOf(pto) == 2) {
	 * Set<DefaultWeightedEdge> aristasPto = grafo.edgesOf(pto); double suma = 0;
	 * for (DefaultWeightedEdge arista : aristasPto) {
	 * 
	 * } } }
	 * 
	 * System.out.println(inspector.getCutpoints().size());
	 * 
	 * return null; }
	 */

	/*
	 * IGNORAR
	 * 
	 * //RENOMBRAMOS LOS PUNTOS public void renombraPuntos() { int cnt = 1;
	 * //contador de IDs for (Punto p : puntos) { p.id = cnt++; } }
	 * 
	 * //Renombra los puntos (su id) a partir de 1 EN LAS ARISTAS, por si hicimos
	 * algo raro public void renombraPuntos2() { Hashtable oldnew = new Hashtable();
	 * int nuevo = 1; for (Arista arista : aristas) { int old = arista.getP1().id;
	 * if (!oldnew.containsKey(old)) { oldnew.put(old, nuevo++); } old =
	 * arista.getP2().id; if (!oldnew.containsKey(old)) { oldnew.put(old, nuevo++);
	 * } } LinkedList<Arista> aristas2 = new LinkedList<>(aristas); for(Arista
	 * arista : aristas2) { arista.getP1().id = (int) oldnew.get(arista.getP1().id);
	 * arista.getP2().id = (int) oldnew.get(arista.getP2().id); } }
	 */

}
